
using GameStore.Api.Data;
using GameStore.Api.Endpoints;
using GameStore.Api.Repositoriesa;
using Job;
using Jobs.Configurations;

using Microsoft.EntityFrameworkCore;

using Quartz;
using Quartz.Impl;
using Quartz.Spi;


var builder = WebApplication.CreateBuilder(args);


// database
// builder.Services.AddScoped<IGamesRepository, InMemGamesRepository>();
// builder.Services.AddSingleton<IGamesRepository, InMemGamesRepository>();
builder.Services.AddScoped<IGamesRepository, EntityFrameworkGamesRepository>();

builder.Services.AddRepositories(builder.Configuration);

// register swagger ui service
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// register quatz
builder.Services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();

// register quatz job
builder.Services.AddSingleton<BatteryJob>();
builder.Services.AddSingleton(new JobSchedule(typeof(BatteryJob), "0/2 * * * * ?"));
builder.Services.AddHostedService<JobHostedService>();

// logger
builder.Logging.ClearProviders();
builder.Logging.AddConsole();

var logger = LoggerFactory.Create(config =>
{
    config.AddConsole();
}).CreateLogger("Program");




var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


await app.Services.InitalizeDb();


app.MapGet("/", (ILogger<Program> logger) =>
{
    logger.LogInformation("hello world");
    return "hello world";
});
// 
app.MapGamesEndpoints();

app.Run();





