```json
{
  "Logging": {
    "LogLevel": { // All providers, LogLevel applies to all the enabled providers.
      "Default": "Error", // Default logging, Error and higher.
      "Microsoft": "Warning" // All Microsoft* categories, Warning and higher.
    },
    "Debug": { // Debug provider.
      "LogLevel": {
        "Default": "Information", // Overrides preceding LogLevel:Default setting.
        "Microsoft.Hosting": "Trace" // Debug:Microsoft.Hosting category.
      }
    },
    "EventSource": { // EventSource provider
      "LogLevel": {
        "Default": "Warning" // All categories of EventSource provider.
      }
    }
  }
}
```

```cs
using Job;
using Quartz;
using Quartz.Impl;


namespace Jobs.Configurationsa;

public class JobScheduler
{
    // public static IServiceCollection AddQuartzService(this IServiceCollection services )
    // {
    //     // config sqlite
    // }
    public static async void Work()
    {
        //调度器工厂
        ISchedulerFactory Work = new StdSchedulerFactory();
        //调度器
        IScheduler schedulers_work = await Work.GetScheduler();

        await schedulers_work.GetJobGroupNames();


        // 创建作业
        IJobDetail job = JobBuilder.Create<BatteryJob>()
            .WithIdentity("job1", "group1")
            .Build();


        // 创建触发器
        ITrigger trigger = TriggerBuilder.Create()
            .WithIdentity("trigger1", "group1")
            .StartNow()
            .WithSimpleSchedule(x => x
                .WithIntervalInSeconds(10)
                .RepeatForever())
            .Build();


        //添加任务及触发器至调度器中
        await schedulers_work.ScheduleJob(job, trigger);

        //启动
        await schedulers_work.Start();
    }
}
```



```cs
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
```
