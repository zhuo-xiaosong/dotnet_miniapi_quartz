using GameStore.Api.Data;
using GameStore.Api.Endpoints;
using GameStore.Api.Repositoriesa;
using Job;
using Jobs.Configurations;

using NLog;
using NLog.Web;
using Quartz;
using Quartz.Impl;

var logger = LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();

var builder = WebApplication.CreateBuilder(args);

// config nlog
builder.Logging.ClearProviders();
builder.Logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
builder.Host.UseNLog();



// database
// builder.Services.AddScoped<IGamesRepository, InMemGamesRepository>();
// builder.Services.AddSingleton<IGamesRepository, InMemGamesRepository>();
builder.Services.AddScoped<IGamesRepository, EntityFrameworkGamesRepository>();

builder.Services.AddRepositories(builder.Configuration);

// register swagger ui service
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// register quatz
builder.Services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();

// register quatz job
builder.Services.AddSingleton<BatteryJob>();
builder.Services.AddSingleton(new JobSchedule(typeof(BatteryJob), "0/2 * * * * ?"));
builder.Services.AddHostedService<JobHostedService>();



var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


await app.Services.InitalizeDb();


app.MapGet("/", (ILogger<Program> logger) =>
{
    logger.LogInformation("hello world");
    logger.LogWarning("test");
    app.Logger.LogInformation("app log");
    app.Logger.LogError("app log");
    return "hello world";
});
// 
app.MapGamesEndpoints();

app.Run();





