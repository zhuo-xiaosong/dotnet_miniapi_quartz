
using Microsoft.EntityFrameworkCore;

namespace GameStore.Api.Data;

public static class DataExtensions
{

    // 扩展 IServiceProvider接口的方法
    public static async Task InitalizeDb(this IServiceProvider provider)
    {
        using var scope = provider.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<GameStoreContext>();
        // dbContext.Database.Migrate();
        await dbContext.Database.MigrateAsync();
    }
    // 扩展IServiceCollection接口的方法
    public static IServiceCollection AddRepositories(this IServiceCollection services, IConfiguration configuration)
    {
        // config sqlite
        services.AddSqlite<GameStoreContext>(configuration.GetConnectionString("DefaultConnection"));
        return services;
    }
}
